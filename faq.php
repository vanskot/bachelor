<!DOCTYPE html>
<?php
include 'connect.php';
include 'header.php';
?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title> Idle Garden </title>
    <link href="css/style.css" media="screen" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'rel='stylesheet' type='text/css'>
</head>
<body>
<div align="center">
<ul class="h2">
    <li class="menu-item">
        <a href="#"><h3>Есть ли дата выхода игры</h3></a>
        <ul class="h2">
            <br>
            <li>
                Дата пока неизвестна, предположительно во втором квартале 2021го года.
            </li>
        </ul>
    </li>
</ul>
<ul class="h3">
    <li class="menu-item">
        <a href="#"><h3>В чем суть игры?</h3></a>
        <ul class="h2">
            <br>
            <li>
                Как и все тайм-киллеры - просто убить время, проводя его весело.
            </li>
        </ul>
    </li>
</ul>
<ul class="h2">
    <li class="menu-item">
        <a href="#"><h3>Какова тематика игры?</h3></a>
        <ul class="h2">
            <br>
            <li>
                На данный момент - Time Killer. Разработка идет для получения опыта в проектировании.
            </li>
        </ul>
    </li>
</ul>
    <ul class="h2">
        <li class="menu-item">
            <a href="#"><h3>Вы так-же можете задать нам свой вопрос и мы вам на него ответим :)</h3></a>
        </li>
    </ul>
</a>
</div>
<form action="main.php" id="questionform" method="post"name="questionform">
    <p><span class="h1"><input type="text" id ="your-name" name="your-name" value="" size="38" class="h1" aria-required="true" aria-invalid="false" placeholder="Введите ваше имя"></span></p>
    <p><span class="h1"><input type="email" id="your-email" name="your-email" value="" size="38" class="h1" aria-required="true" aria-invalid="false" placeholder="Введите ваш Email"></span></p>
    <p class="full-width"><span class="h2"><textarea id="your-message" name="your-message" maxlength="299" cols="40" rows="10" class="h3" aria-invalid="false" placeholder="Сообщение"></textarea></span></p>
    <p><input type="submit" id="your-question" name="your-question" value="Отправить" class="h1"><span class="ajax-loader"></span></p></form></div>
</body></html>
<script>
				var el = document.getElementsByClassName('menu-item');
				for(var i=0; i<el.length; i++) {
    el[i].addEventListener("mouseenter", showSub, false);
    el[i].addEventListener("mouseleave", hideSub, false);
}
				function showSub(e) {
                    if(this.children.length>1) {
                        this.children[1].style.height = "auto";
                        this.children[1].style.overflow = "visible";
                        this.children[1].style.opacity = "1";
                    } else {
                        return false;
                    }
                }function hideSub(e) {
    if(this.children.length>1) {
        this.children[1].style.height = "0px";
        this.children[1].style.overflow = "hidden";
        this.children[1].style.opacity = "0";
    } else {
        return false;
    }
}
</script>
<?php include("footer.php"); ?>