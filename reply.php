<?php
//reply.php
include 'connect.php';
include 'header.php';

if($_SERVER['REQUEST_METHOD'] != 'POST')
{
    echo 'This file cannot be called directly.';
}
else
{
    if(!$_SESSION['signed_in'])
    {
        echo 'You must be signed in to post a reply.';
    }
    else
    {
        $test = mysqli_real_escape_string($conn, $_GET['id']);
        echo (mysqli_real_escape_string($conn, $_GET['id']));
        $sql = "INSERT INTO posts(post_content, post_date, post_topic, post_by)
                VALUES ('" . $_POST['reply-content'] . "', NOW(),
                        " . "4" . ",
                        " . $_SESSION['user_id'] . ")";

        $result = mysqli_query($conn, $sql);

        if(!$result)
        {
            echo (mysqli_error($conn));
        }
        else
        {
            echo 'Your reply has been saved, check out <a href="topic.php?id=' . htmlentities($_GET['id']) . '">the topic</a>.';
        }
    }
}
include 'footer.php';
?>
